@extends('master-html')

@section('title')
        Sanju XYZ | Home
@stop

@section('content')
        <!-- Start WOWSlider.com BODY section -->
    <div id="wowslider-container1">
        <div class="ws_images"><ul>
                <li><img src="/slideshow/data1/images/room.jpg" alt="" title="" id="wows1_0"/></li>
                <li><img src="/slideshow/data1/images/food.jpg" alt="food" title="food" id="wows1_1"/></li>
                <li><a href="http://wowslider.com"><img src="/slideshow/data1/images/gym.jpg" alt="bootstrap carousel" title="gym" id="wows1_2"/></a></li>
                <li><img src="/slideshow/data1/images/pool.jpg" alt="pool" title="pool" id="wows1_3"/></li>
            </ul></div>
        <div class="ws_bullets"><div>
                <a href="#" title=""><span><img src="/slideshow/data1/tooltips/room.jpg" alt=""/>1</span></a>
                <a href="#" title="food"><span><img src="/slideshow/data1/tooltips/food.jpg" alt="food"/>2</span></a>
                <a href="#" title="gym"><span><img src="/slideshow/data1/tooltips/gym.jpg" alt="gym"/>3</span></a>
                <a href="#" title="pool"><span><img src="/slideshow/data1/tooltips/pool.jpg" alt="pool"/>4</span></a>
            </div></div><div class="ws_script" style="position:absolute;left:-99%"><a href="http://wowslider.net">jquery carousel</a> by WOWSlider.com v8.5</div>
        <div class="ws_shadow"></div>
    </div>
    <script type="text/javascript" src="/slideshow/engine1/wowslider.js"></script>
    <script type="text/javascript" src="/slideshow/engine1/script.js"></script>
    <!-- End WOWSlider.com BODY section -->



@stop

@section('package')
    <div class="containerPackage">
        <h2>Packages on offer</h2>
        <p>You can book the one which best suits you</p>
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Services</th>
                <th>Half board</th>
                <th>Full board</th>
                <th>Premium</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Breakfast</td>
                <td>&#10007;</td>
                <td>&#10003;</td>
                <td>&#10003;</td>
            </tr>
            <tr>
                <td>Room service</td>
                <td>&#10003;</td>
                <td>&#10003;</td>
                <td>&#10003;</td>
            </tr>
            <tr>
                <td>Access to gym</td>
                <td>&#10003;</td>
                <td>&#10003;</td>
                <td>&#10003;</td>
            </tr>
            <tr>
                <td>Access to pool</td>
                <td>&#10003;</td>
                <td>&#10003;</td>
                <td>&#10003;</td>
            </tr>
            <tr>
                <td>Access to entertainment facilities</td>
                <td>&#10003;</td>
                <td>&#10003;</td>
                <td>&#10003;</td>
            </tr>
            <tr>
                <td>Fully furnished room and bathroom</td>
                <td>&#10007;</td>
                <td>&#10007;</td>
                <td>&#10003;</td>
            </tr>
            <tr>
                <td>Priority issue handling</td>
                <td>&#10007;</td>
                <td>&#10007;</td>
                <td>&#10003;</td>
            </tr>

            </tbody>
        </table>
    </div>

    <button type="button" class="btn TryIt" onclick="alert('You chose Half board');">Try it</button>
    <button type="button" class="btn LikeIt" onclick="alert('You chose Full board');">Like it</button>
    <button type="button" class="btn LoveIt" onclick="alert('You chose Premium');">Love it</button>

@stop