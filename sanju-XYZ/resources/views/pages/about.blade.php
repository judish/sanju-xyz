@extends('master-html')

@section('title')
    Sanju XYZ | About
@stop

@section('content')

    <h1 id="title">Welcome to Sanju XYZ Hotel</h1>

    <p class="aboutText">
        Sanju XYZ is a leader in the global hospitality industry, with a distinctive collection and a worldwide reputation for excellence. Our diverse portfolio includes historic icons, elegant resorts and modern city center properties. From the beaches of Blue Bay and Flic en Flac to the heart of Mauritius, all of our hotels offer a superior guest experience that is uniquely ''Sanju XYZ".
    </p>
    <p class="aboutText">
        Hotels under the Sanju XYZ banner offer guests an extraordinary place that is created by combining unique architecture and structure, expressive decor and artistry, and magnificent features all in one great location. Add great service to this and the result is an extraordinary experience that would make your memory of Sanju XYZ Hotels a long and lasting one. Most of our hotels were among the first buildings to be erected in young cities across Mauritius. Few hotels can boast that their communities literally grew up around them. Today, we have added modern city center properties to our collection, with core locations that allow you to join the hustle and bustle of the city, take part in the action of the business district, and enjoy the culture, the lights and sounds of street life.
    </p>

@stop
