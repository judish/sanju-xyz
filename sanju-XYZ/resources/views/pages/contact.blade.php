@extends('master-html')

@section('title')
    Sanju XYZ | Contact
@stop

@section('content')

    <h1 id="title">Contact Us</h1>

    <div id="content">
        <section id="info">
            <h3>Address: </h3>
            143, Marshalon Street,
            <br/>
            Rose-hill,
            <br/>
            Mauritius
            <br/>
            <p><h3>Tel No: </h3> (+230) 654-9876</p>

            <p><h3>Email: </h3><a href="mailto:sanjuxyz@gmail.com">sanjuxya@gmail.com</a></p>
            <br/>
        </section>

        <section id="map">
            <iframe  alt="san-xyz-map" width="500px" height="320px" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d29932.621759060286!2d57.522939350000016!3d-20.317738899999977!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x217c5c4b87fb8e89%3A0x865d0c09a4bfceb6!2sCurepipe!5e0!3m2!1sen!2smu!4v1411738183681" ></iframe>
        </section>

    </div>

@stop