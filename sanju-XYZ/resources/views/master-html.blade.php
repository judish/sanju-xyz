<!doctype html>

<html lang="en"></html>

<head>

    <meta charset="UTF-8">

    <title>@yield('title')</title>

    <link href="{{ asset('/css/main.css') }}" rel="stylesheet" type="text/css" >
    <link href="{{ asset('/css/home.css') }}" rel="stylesheet" type="text/css" >
    <link href="{{ asset('/css/about.css') }}" rel="stylesheet" type="text/css" >
    <script src="/functions/showDialog.js"></script>
    <script src="/functions/validate_login.js"></script>
    <script src="http://s.codepen.io/assets/libs/modernizr.js" type="text/javascript"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <!-- Start WOWSlider.com HEAD section -->
    <link href="{{ asset('/slideshow/engine1/style.css') }}" rel="stylesheet" type="text/css" >
    <script src="/slideshow/engine1/jquery.js" type="text/javascript"></script>
    <!-- End WOWSlider.com HEAD section -->


</head>

<body>

    <header>

        <blockquote>
            If anything truly defines Sanju XYZ, it is the value we attach to lasting memories. Building upon a century of experience as hoteliers has . . .
        </blockquote>

        <section id="logo">
            <div id="leftNote">
                <h1 style="color:#8B4513">Hotel</h1>
            </div>
            <img src="create_thumb.png" alt="logo">
            <div id="rightNote">
                <h1 style="color:#8B4513">You're in the right place...</h1>
            </div>
        </section>


        <button class="loginButton" id="loginButton" type="button" onclick="show_dialog('containerlogin');">Log In</button>

        <nav>
            <button class="homeButton" type="button"
                    onclick="window.location.href='/'">Home</button>

            <button class="contactButton" type="button"
                    onclick="window.location.href='/contact'">Contact Us</button>

            <button class="aboutButton" type="button"
                    onclick="window.location.href='/about'">About</button>
        </nav>

    </header>


    <div class="containerlogin" id="containerlogin">
        <div class="log-in-dialog" id="log-in">
            <header class="log-in-header">
                <span class="header-title">LOGIN</span>
            </header>
            <section class="dialog-body" id="dialog-body">
                <div id="errors" style="color:red"></div>
                <form action="#" method="">
                    <label>Username</label>
                    <br/>
                    <input type="text" size="35" name="username" autofocus/>
                    <br/><br/>
                    <label>Password</label>
                    <br/>
                    <input type="password" size="35" name="password"/>
                    <div class="btns">
                        <div class="btnLeft">
                            <a href="javascript:void(0)" onclick="show_dialog('containerlogin');">Cancel</a>
                        </div>
                        <div class="btnRight">
                            <input type="button" value="Log In" onclick="validate_login(this.form.username.value,this.form.password.value);"/>
                        </div>
                    </div>
                </form>
                <br/>
                <a href="javascript:void(0)" class="forgotPassword" onclick="show_dialog('containerpass')">Forgot password?</a>
            </section>
        </div>
    </div>

    <container>

        @yield('content')

    </container>

    <container>

        @yield('package')

    </container>

    <footer>
        <div id="contents">
            <p>
            © Copyright 2015 Sanju XYZ. All rights reserved.
            </p>
            <p>
            Privacy Policy | Terms & Conditions | News
            </p>
        </div>
    </footer>

</body>

</html>