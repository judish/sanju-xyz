function show_dialog(element) {
    var dialog = document.getElementById(element);

    if (dialog.style.display == 'block')
        dialog.style.display = 'none';
    else
        dialog.style.display = 'block';
}
